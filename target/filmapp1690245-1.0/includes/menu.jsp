<%--
  Created by IntelliJ IDEA.
  User: Kevin Veld
  Date: 8-6-2016
  Time: 14:15
  To change this template use File | Settings | File Templates.
--%>
<div class ="menu">
    <ul>
        <c:if test="${gebruiker != null}">
            <li><a href="/gebruiker/FilmDB.jsp">FilmDB</a></li>
            <c:if test="${gebruiker.beheerder_status==1}">
                <li class="dropdown"><a href="#" class="dropbtn">Tools</a>
                    <ul class="dropdown-content">
                        <li><a href="/gebruiker/admin/addfilm.jsp">Film toevoegen</a></li>
                        <li><a href="/gebruiker/admin/editfilm.jsp">Film wijzigen</a></li>
                        <li><a href="/gebruiker/admin/deletefilm.jsp">Film verwijderen</a></li>
                        <li><a href="/gebruiker/admin/adduser.jsp">User toevoegen</a></li>
                    </ul>
                </li>
            </c:if>
            <li><a href="/gebruiker/watchlist.jsp">Watchlist</a></li>
            <li><a href="/LogoutServlet.do">Logout</a></li>
        </c:if>
    </ul>
</div>