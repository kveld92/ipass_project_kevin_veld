<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Kevin Veld
  Date: 26-4-2016
  Time: 00:38
  To change this template use File | Settings | File Templates.

--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Home</title>
  <link rel="stylesheet" type="text/css" href="/css/reset.css">
  <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
<%@ include file="/includes/banner.jsp" %>
<c:choose>
  <c:when test="${gebruiker == null}">
    <div class="login">
      <img id="user_icon" src="images/user.png" alt="user_icon"/>
      <h2>Login</h2>
      <form action="LoginServlet.do" method="POST">
        Gebruikersnaam: </br> <input type="text" name="uname" pattern="^[a-zA-Z0-9]*" maxlength="15" required="required"/> </br>
        Wachtwoord:  </br> <input type="password" name="pass" pattern="^[a-zA-Z0-9]*" maxlength="15" required="required"/> </br>
        </br> <input type="submit" value="Login"/>
      </form>
    </div></br>
  </c:when>
  <c:otherwise>
    <div class = "post">
      Welkom <c:out value="${gebruiker.gebruikersnaam}"/>
      <c:if test="${gebruiker.beheerder_status==1}">
        </br></br>
        <p> Omdat u in bezit bent van een beheerder status zijn de volgende services voor u beschikbaar:
          </br>
          </br> - films toevoegen
          </br> - films wijzigen
          </br> - films verwijderen
          </br> - gebruikers toevoegen
          </br> - films toevoegen aan de watchlist
          </br>
          </br> Heel veel succes met de FilmApp!
        </p>
      </c:if>
      <c:if test="${gebruiker.beheerder_status==0}">
        </br></br>
        <p> Omdat u gebruik maakt van een kijkers account zijn de volgende services voor u beschikbaar:
          </br>
          </br> - films toevoegen aan de watchlist
          </br> - films beoordelen
          </br>
          </br> Heel veel succes met de FilmApp!
        </p>
      </c:if>
    </div>
  </c:otherwise>
</c:choose>
</body>
</html>
