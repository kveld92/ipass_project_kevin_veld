<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kevin Veld
  Date: 9-6-2016
  Time: 00:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>FilmDB</title>
    <link rel="stylesheet" type="text/css" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
    <%@ include file="/includes/banner.jsp" %>
    <div class ="post">
        <h2>FilmDB</h2>
        </br></br>
        <div class="search_bar">
            <form action="/gebruiker/FilmServletSearch.do" method="POST">
                <input id="search_bar_input"size="20" type="search" name="searchbar" pattern="^[a-zA-Z0-9 ]*" required="required"/> Zoeken op:
                <select  name="zoeken_op">
                    <option value="naam">Titel</option>
                    <option value="jaartal">Jaartal</option>
                    <option value="genre">Genre</option>
                    <option value="samenvatting">Samenvatting</option>
                </select>
                <input id="search_bar_button" type="submit" value="Zoeken"/>
            </form>
        </div>
        </br></br>
        <table>
            <tr><th>Titel</th><th>Jaartal</th><th>Genre</th><th>BoxNr</th><th>PlaatsNr</th><th>Overzicht</th></tr>
            <form action="/gebruiker/FilmServletDetail.do" method="POST">
            <c:forEach var="i" items="${film}">
                <tr><td><c:out value="${i.naam}"/></td><td><c:out value="${i.jaartal}"/></td><td><c:out value="${i.genre}"/></td><td><c:out value="${i.box_nr}"/></td><td><c:out value="${i.plaats_nr}"/></td><td><button id="detail_button" name="titel" type="submit" value="<c:out value="${i.naam}"/>">details</button></td></tr>
            </c:forEach>
            </form>
        </table>
    </div>
</body>
</html>