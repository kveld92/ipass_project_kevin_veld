<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kevin Veld
  Date: 9-6-2016
  Time: 20:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Film toevoegen</title>
    <link rel="stylesheet" type="text/css" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <script src="https://code.jquery.com/jquery-2.2.3.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#filmadd_form').submit(function() {
                return confirm("U staat op het punt een film toe te voegen. Druk op OK om door te gaan");
            });
        });
    </script>
</head>
<body>
<%@ include file="/includes/banner.jsp" %>
    <div class = "container1">
        <h2>Film toevoegen</h2>
        <p> Vul de gegevens van de film toe die u wilt toevoegen: </p > </br>
        <div class ="film_form">
            <form id="filmadd_form" action="/gebruiker/FilmServletAdd.do" method="POST">
                Naam: </br> <input type="text" name="naam" maxlength="45"  pattern="^[a-zA-Z0-9 :!-]*" required="required"/> </br>
                Jaartal:  </br> <input type="number" min = 0 name="jaartal" maxlength="4" required="required"/> </br>
                Genre:  </br> <input type="text" name="genre"  maxlength="45"  pattern="^[a-zA-Z0-9 ]*" required="required"/> </br>
                BoxNr:  </br> <input type="number" min = 0 name="box_nr" required="required"/> </br>
                PlaatsNr:  </br> <input type="number" min = 0 name="plaats_nr" required="required"/> </br>
                Samenvatting:  </br>  <textarea rows ="5" cols="25" name="samenvatting" maxlength="254"  pattern="^[a-zA-Z0-9 ]*" required="required"></textarea></br>
                </br><input type="submit" value="Toevoegen"/>
            </form>
        </div>
    </div>
</body>
</html>
