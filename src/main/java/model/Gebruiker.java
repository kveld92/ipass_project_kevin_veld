package model;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin Veld on 7-6-2016.
 */
public class Gebruiker {
    private int gebruiker_id;
    private String gebruikersnaam;
    private String wachtwoord;
    private int beheerder_status;
    List<Watch> watchlist;

    public Gebruiker(String gebruikersnaam, String wachtwoord, int beheerder_status){
        watchlist = new ArrayList<Watch>();
        this.gebruikersnaam = gebruikersnaam;
        this.beheerder_status = beheerder_status;
        this.wachtwoord = wachtwoord;
    }

    public int getGebruiker_id(){ return gebruiker_id; }

    public void setGebruiker_id(int gebruiker_id){ this.gebruiker_id = gebruiker_id;}

    public String getGebruikersnaam(){return gebruikersnaam; }

    public int getBeheerder_status(){return beheerder_status; }

    public boolean checkPassword(String pw){
        boolean bool = false;
        if(pw.equals(wachtwoord)){
            bool = true;
        }
        return bool;
    }

    public String getWachtwoord(){return wachtwoord;}

    public void setWatchlist(List<Watch> watchlist){this.watchlist = watchlist;}

    public List<Watch> getWatchlist(){return watchlist;}

    public String toString(){
        return gebruiker_id + " / " +gebruikersnaam +" / "+ wachtwoord + " / " + beheerder_status + "\n"; }
}
