package model;

/**
 * Created by Kevin Veld on 7-6-2016.
 */
public class Film {
    private int film_id;
    private String naam;
    private int jaartal;
    private String genre;
    private String samenvatting;
    private int box_nr;
    private int plaats_nr;

    public Film(String naam, int jaartal, String genre, String samenvatting, int box_nr, int plaats_nr){
        this.naam = naam;
        this.jaartal = jaartal;
        this.genre = genre;
        this.samenvatting = samenvatting;
        this.box_nr = box_nr;
        this.plaats_nr = plaats_nr;
    }

    public int getFilm_id(){ return film_id; }

    public void setFilm_id(int film_id){ this.film_id = film_id; }

    public String getNaam(){ return naam; }

    public int getJaartal(){ return jaartal; }

    public String getGenre(){ return genre; }

    public String getSamenvatting(){ return samenvatting; }

    public int getBox_nr(){ return box_nr; }

    public int getPlaats_nr(){ return plaats_nr; }

    public String toString(){
        return  film_id + " / " + naam + " / "+ jaartal +" / "+ genre + " / "+samenvatting + " / "+box_nr + " / "+plaats_nr + "\n";
    }
}
