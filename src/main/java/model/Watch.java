package model;

/**
 * Created by Kevin Veld on 7-6-2016.
 */
public class Watch {
    private int gebruiker_id;
    private int watch_id;
    private int film_id;
    private String watch_status;

    public Watch(int watch_id, int gebruiker_id){
        this.watch_id = watch_id;
        this.gebruiker_id = gebruiker_id;
    }

    public void setFilm_id(int film_id) {this.film_id = film_id; }

    public int getFilm_id(){ return film_id; }

    public int getGebruiker_id(){ return gebruiker_id; }

    public int getWatch_id(){ return watch_id; }

    public void setWatch_status(String watch_status){
        this.watch_status = watch_status;
    }

    public String getWatch_status(){
        return watch_status;
    }

    public String toString(){
        return gebruiker_id + " / " + film_id +" / " + watch_status + " / ";
    }
}
