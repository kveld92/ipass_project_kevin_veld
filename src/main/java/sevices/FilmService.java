package sevices;

import dao.FilmDAO;
import dao.GebruikerDAO;
import dao.WatchDAO;
import model.Film;
import model.Gebruiker;
import model.Watch;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Kevin Veld on 8-6-2016.
 */
public class FilmService {
    FilmDAO filmDAO;
    GebruikerDAO gebruikerDAO;
    WatchDAO watchDAO;

    private List<Watch> watchlist;
    private List<Gebruiker> gebruikers;
    private List<Film> films;

    public FilmService(){
        gebruikerDAO = new GebruikerDAO();
        filmDAO = new FilmDAO();
        watchDAO = new WatchDAO();
        this.films = filmDAO.getAllFilms();
        this.gebruikers = gebruikerDAO.getAllGebruikers();
        this.watchlist = watchDAO.getWatchLists();

        for(Gebruiker g : gebruikers){
            List<Watch> temp = new ArrayList<Watch>();
            for(Watch w : watchlist){
                if(g.getGebruiker_id()== w.getGebruiker_id()){
                    temp.add(w);
                }
            }
            g.setWatchlist(temp);
        }
    }

    public List<Gebruiker> getAllGebruikers(){
        this.gebruikers = gebruikerDAO.getAllGebruikers();
        return gebruikers;
    }

    public void addWatch(String watch_status, int gebruiker_id, int film_id){
        watchDAO.addWatch(watch_status, gebruiker_id , film_id);
    }

    public void updateWatch(String watchstatus, int watch_id, int film_id){
        watchDAO.updateWatch(watchstatus, watch_id, film_id);
    }

    public List<Film> getAllFilms() {
        this.films = filmDAO.getAllFilms();
        return films;
    }

    public List<Watch> getWatchlists(){
        this.watchlist = watchDAO.getWatchLists();
        return watchlist;
    }

    public List<Film> SearchFilm(String s, String s2){ return filmDAO.getFilmByName(s, s2); }

    public void deleteFilm(String s) {filmDAO.deleteFilm(s);}

    public void updateFilm(Film f){filmDAO.updateFilm(f);}

    public void AddFilm(Film f){filmDAO.addFilm(f);}

    public void AddUser(Gebruiker g){
        gebruikerDAO.addGebruiker(g);
    }

    public List<String> checkUser(String uname, String pass, String pass2){
        List<String> errors = new ArrayList<String>();
        if(uname.isEmpty() || pass.isEmpty()){
            errors.add(new String("Er mogen geen velden leeg zijn"));
        }
        else{
            if(!pass.equals(pass2)){
                errors.add(new String("Wachtwoorden komen niet overeen"));
            }
            else {
                for (Gebruiker g : gebruikers) {
                    if (uname.equals(g.getGebruikersnaam())) {
                        errors.add(new String("Gebruiker bestaat al"));
                    }
                }
            }
        }
        return errors;
    }

    public List<String> checkFilm(String naam, String jaartal, String genre, String samenvatting, String box_nr, String plaats_nr, boolean ck_name_exist, boolean ck_pos_exist){
        int year = Calendar.getInstance().get(Calendar.YEAR);
        List<String> errors = new ArrayList<String>();
        if (naam.isEmpty() || jaartal.isEmpty() || genre.isEmpty() || samenvatting.isEmpty() || box_nr.isEmpty() || plaats_nr.isEmpty()) {
            errors.add(new String("Er mogen geen velden leeg zijn"));
        } else {
            try {
                int jaar_fmt = Integer.parseInt(jaartal);
                if (jaar_fmt < 1900 || jaar_fmt > year) {
                    errors.add(new String("Jaartal moet tussen 1900 en " + String.valueOf(year) + " liggen"));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            for (Film f : films) {
                if (ck_name_exist) {
                    if (f.getNaam().equals(naam)) {
                        errors.add(new String("Film " + naam + " bestaat al"));
                        break;
                    }
                }
                if (ck_pos_exist) {
                    if (f.getBox_nr() == Integer.parseInt(box_nr) && f.getPlaats_nr() == Integer.parseInt(plaats_nr)) {
                        errors.add(new String("Box: " + box_nr + " op plaats nummer " + plaats_nr + " is al bezet"));
                        break;
                    }
                }
            }
        }
        return errors;
    }

    public Gebruiker loginUser(String username, String password){
        Gebruiker u = null;
        gebruikers = gebruikerDAO.getAllGebruikers();
        for(Gebruiker gebruiker : gebruikers){
            if(gebruiker.getGebruikersnaam().equals(username) && gebruiker.checkPassword(password)){
                u = gebruiker;
                break;
            }
        }
        return u;
    }
}
