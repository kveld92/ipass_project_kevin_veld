package sevices;

/**
 * Created by Kevin Veld on 8-6-2016.
 */
public class SeviceProvider {
    private static FilmService filmService = new FilmService();

    public static FilmService getFilmService(){ return filmService; }
}
