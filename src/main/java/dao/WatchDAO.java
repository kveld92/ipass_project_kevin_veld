package dao;

import model.Watch;
import utils.DBUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin Veld on 14-6-2016.
 */
public class WatchDAO {
    private Connection conn;

    public WatchDAO(){ conn = DBUtil.getConnection();}

    public void addWatch(String watch_status, int gebruiker_id, int film_id){
        try{
            PreparedStatement preparedStatement = conn
                    .prepareStatement("insert into watchlist(film_id, gebruikers_id, watch_status) values (?,?,?)");
            preparedStatement.setInt(1, film_id);
            preparedStatement.setInt(2, gebruiker_id);
            preparedStatement.setString(3, watch_status);
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void updateWatch(String watch_status, int gebruiker_id, int film_id) {
        try {
            PreparedStatement preparedStatement = conn
                    .prepareStatement("update watchlist set watch_status=? where gebruikers_id=? AND film_id=?");
            preparedStatement.setString(1,watch_status);
            preparedStatement.setInt(2, gebruiker_id);
            preparedStatement.setInt(3, film_id);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Watch> getWatchLists(){
        List<Watch> watchlist = new ArrayList<Watch>();
        try{
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("select * from watchlist");

            while(rs.next()){
                Watch watch = new Watch(rs.getInt("watch_id"), rs.getInt("gebruikers_id"));
                watch.setFilm_id(rs.getInt("film_id"));
                watch.setWatch_status(rs.getString("watch_status"));
                watchlist.add(watch);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return watchlist;
    }
}
