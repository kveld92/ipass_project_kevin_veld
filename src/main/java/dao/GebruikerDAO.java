package dao;

import model.Gebruiker;
import utils.DBUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin Veld on 8-6-2016.
 */
public class GebruikerDAO {
    private Connection conn;

    public GebruikerDAO(){
        conn = DBUtil.getConnection();
    }

    public void addGebruiker(Gebruiker g){
        try{
            PreparedStatement preparedStatement = conn
                    .prepareStatement("insert into gebruiker(gebruikersnaam, wachtwoord, beheerder_status) values (?, ?, ?)");
            preparedStatement.setString(1,g.getGebruikersnaam());
            preparedStatement.setString(2,g.getWachtwoord());
            preparedStatement.setInt(3, g.getBeheerder_status());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void deleteGebruiker(String gebruikersnaam){
        try{
            PreparedStatement preparedStatement = conn
                    .prepareStatement("delete from gebruiker where gebruikersnaam=?");
            preparedStatement.setString(1,gebruikersnaam);
            preparedStatement.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    public List<Gebruiker> getAllGebruikers() {
        List<Gebruiker> users = new ArrayList<Gebruiker>();
        try {
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("select * from gebruiker");
            while (rs.next()) {
                Gebruiker user = new Gebruiker(rs.getString("gebruikersnaam"), rs.getString("wachtwoord"),rs.getInt("beheerder_status"));
                user.setGebruiker_id(rs.getInt("gebruikers_id"));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

}
