package dao;

import model.Film;
import utils.DBUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin Veld on 9-6-2016.
 */
public class FilmDAO {
    private Connection conn;

    public FilmDAO(){ conn = DBUtil.getConnection(); }

    public void addFilm(Film f){
        try{
            PreparedStatement preparedStatement = conn
                    .prepareStatement("insert into film(naam, jaartal, genre, samenvatting, box_nr, plaats_nr) values(?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, f.getNaam());
            preparedStatement.setInt(2, f.getJaartal());
            preparedStatement.setString(3,f.getGenre());
            preparedStatement.setString(4,f.getSamenvatting());
            preparedStatement.setInt(5, f.getBox_nr());
            preparedStatement.setInt(6, f.getPlaats_nr());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void deleteFilm(String naam){
        try{
            PreparedStatement preparedStatement = conn
                    .prepareStatement("delete from film where naam=?");
            preparedStatement.setString(1,naam);
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void updateFilm(Film f){
        try{
            PreparedStatement preparedStatement = conn
                    .prepareStatement("update film set naam=?, jaartal=?, genre=?, samenvatting=?, box_nr=?, plaats_nr=? where naam = ?");
            preparedStatement.setString(1,f.getNaam());
            preparedStatement.setInt(2,f.getJaartal());
            preparedStatement.setString(3,f.getGenre());
            preparedStatement.setString(4, f.getSamenvatting());
            preparedStatement.setInt(5, f.getBox_nr());
            preparedStatement.setInt(6, f.getPlaats_nr());
            preparedStatement.setString(7, f.getNaam());
            preparedStatement.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    public List<Film> getFilmByName(String naam, String zoeken_op){
        List<Film> films = new ArrayList<Film>();
        try{
            if(zoeken_op.equals("naam")) {
                PreparedStatement preparedStatement = conn
                        .prepareStatement("select * from film where naam like ?");
                preparedStatement.setString(1, "%" + naam + "%");
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    Film film = new Film(rs.getString("naam"), rs.getInt("jaartal"), rs.getString("genre"), rs.getString("samenvatting"), rs.getInt("box_nr"), rs.getInt("plaats_nr"));
                    film.setFilm_id(rs.getInt("film_id"));
                    films.add(film);
                }
            }
            if(zoeken_op.equals("jaartal")) {
                PreparedStatement preparedStatement = conn
                        .prepareStatement("select * from film where jaartal like ?");
                preparedStatement.setString(1, "%" + naam + "%");
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    Film film = new Film(rs.getString("naam"), rs.getInt("jaartal"), rs.getString("genre"), rs.getString("samenvatting"), rs.getInt("box_nr"), rs.getInt("plaats_nr"));
                    film.setFilm_id(rs.getInt("film_id"));
                    films.add(film);
                }
            }
            if(zoeken_op.equals("genre")) {
                PreparedStatement preparedStatement = conn
                        .prepareStatement("select * from film where genre like ?");
                preparedStatement.setString(1, "%" + naam + "%");
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    Film film = new Film(rs.getString("naam"), rs.getInt("jaartal"), rs.getString("genre"), rs.getString("samenvatting"), rs.getInt("box_nr"), rs.getInt("plaats_nr"));
                    film.setFilm_id(rs.getInt("film_id"));
                    films.add(film);
                }
            }
            if(zoeken_op.equals("samenvatting")) {
                PreparedStatement preparedStatement = conn
                        .prepareStatement("select * from film where samenvatting like ?");
                preparedStatement.setString(1, "%" + naam + "%");
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    Film film = new Film(rs.getString("naam"), rs.getInt("jaartal"), rs.getString("genre"), rs.getString("samenvatting"), rs.getInt("box_nr"), rs.getInt("plaats_nr"));
                    film.setFilm_id(rs.getInt("film_id"));
                    films.add(film);
                }
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
        return films;
    }
    public List<Film> getAllFilms(){
        List<Film> films = new ArrayList<Film>();
        try{
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("select * from film order by box_nr, plaats_nr");
            while(rs.next()){
                Film film = new Film(rs.getString("naam"), rs.getInt("jaartal"), rs.getString("genre"), rs.getString("samenvatting"), rs.getInt("box_nr"), rs.getInt("plaats_nr"));
                film.setFilm_id(rs.getInt("film_id"));
                films.add(film);
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
        return films;
    }
}
