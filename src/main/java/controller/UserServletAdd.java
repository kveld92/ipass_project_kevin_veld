package controller;

import model.Gebruiker;
import sevices.FilmService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Kevin Veld on 13-6-2016.
 */

public class UserServletAdd extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        FilmService filmService = new FilmService();
        List<String> errors;
        String naam = req.getParameter("uname");
        String wachtwoord = req.getParameter("pass");
        String wachtwoord2 = req.getParameter("pass2");
        String type = req.getParameter("user_type");

        PrintWriter out = resp.getWriter();
        resp.setContentType("text/html");

        errors = filmService.checkUser(naam, wachtwoord, wachtwoord2);

        if(errors.isEmpty()){
            try{
                RequestDispatcher rd = req.getRequestDispatcher("/gebruiker/FilmDB.jsp");
                out.println("<p class=\"message\" > De gebruiker" + naam + " is toegevoegd aan de database</p>");
                filmService.AddUser(new Gebruiker(naam, wachtwoord, Integer.parseInt(type)));
                rd.forward(req, resp);
            } catch(Exception e){
                e.printStackTrace();
            }

        }else{
            RequestDispatcher rd = req.getRequestDispatcher("/gebruiker/admin/adduser.jsp");
            for (String e : errors) {
                out.println("<p class=\"message\" >" + e.toString() + "</p>");
            }

            rd.include(req, resp);
        }
    }
}
