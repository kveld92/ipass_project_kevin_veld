package controller;

import model.Film;
import sevices.FilmService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


/**
 * Created by Kevin Veld on 9-6-2016.
 */
public class FilmServletAdd extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FilmService filmService = new FilmService();
        List<String> errors;
        String naam = req.getParameter("naam").trim();
        String jaartal = req.getParameter("jaartal").trim();
        String genre = req.getParameter("genre").trim();
        String samenvatting = req.getParameter("samenvatting").trim();
        String box_nr = req.getParameter("box_nr").trim();
        String plaats_nr = req.getParameter("plaats_nr").trim();

        PrintWriter out = resp.getWriter();
        resp.setContentType("text/html");

        errors = filmService.checkFilm(naam, jaartal, genre, samenvatting, box_nr, plaats_nr, true, true);

        if (errors.isEmpty()) {
            try {
                RequestDispatcher rd = req.getRequestDispatcher("/gebruiker/FilmDB.jsp");
                out.println("<p class=\"message\" > De film " + naam + " is toegevoegd aan de database</p>");
                filmService.AddFilm(new Film(naam, Integer.parseInt(jaartal), genre, samenvatting, Integer.parseInt(box_nr), Integer.parseInt(plaats_nr)));
                List<Film> f = filmService.getAllFilms();
                req.getSession().setAttribute("film", f);
                rd.forward(req, resp);
            }
            catch(Exception e){
                e.printStackTrace();
            }
        } else {
            RequestDispatcher rd = req.getRequestDispatcher("/gebruiker/admin/addfilm.jsp");
            for (String e : errors) {
                out.println("<p class=\"message\" >" + e.toString() + "</p>");
            }

            rd.include(req, resp);
        }
        out.close();
    }
}
