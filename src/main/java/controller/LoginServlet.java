package controller;

import model.Film;
import model.Watch;
import sevices.FilmService;
import model.Gebruiker;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin Veld on 8-6-2016.
 */
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uname = req.getParameter("uname");
        String pass = req.getParameter("pass");

        PrintWriter out = resp.getWriter();
        resp.setContentType("text/html");

        if(uname.isEmpty() || pass.isEmpty()){
            RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
            out.println("<p  class=\"message\" >Er mogen geen velden leeg zijn</p>");
            out.flush();
            rd.include(req, resp);
        }
        else {
            FilmService service = (FilmService) getServletContext().getAttribute("userlist");
            Gebruiker u = service.loginUser(uname, pass);

            if (u != null) {
                FilmService filmService = new FilmService();
                List<Film> f = filmService.getAllFilms();
                List<Watch> userWatchlist = new ArrayList<Watch>();
                for(Watch w : filmService.getWatchlists()){
                    if(w.getGebruiker_id() == u.getGebruiker_id()){
                        userWatchlist.add(w);
                    }
                }
                RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
                req.getSession().setAttribute("film", f);
                req.getSession().setAttribute("gebruiker", u);
                req.getSession().setAttribute("watchlist", userWatchlist);
                rd.forward(req, resp);
            }
            else {
                RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
                out.println("<p  class=\"message\" >Gebruikersnaam en/of wachtwoord is onjuist</p>");
                out.flush();
                rd.include(req, resp);
            }
        }
    }
}

