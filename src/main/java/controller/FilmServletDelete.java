package controller;

import model.Film;
import sevices.FilmService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Kevin Veld on 12-6-2016.
 */
public class FilmServletDelete extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String cb_del = req.getParameter("cb_del");
        FilmService filmService = new FilmService();
        PrintWriter out = resp.getWriter();
        resp.setContentType("text/html");

        if(cb_del.isEmpty() || cb_del.equals("")){
            RequestDispatcher rd = req.getRequestDispatcher("/gebruiker/admin/deletefilm.jsp");
            out.println("<p class=\"message\">Er is nog geen film gekozen</p>");
            rd.include(req, resp);

        }else {
            try {
                filmService.deleteFilm(cb_del);
                RequestDispatcher rd = req.getRequestDispatcher("/gebruiker/FilmDB.jsp");
                out.println("<p class=\"message\"> De film " + cb_del + " is verwijderd</p>");
                List<Film> f = filmService.getAllFilms();
                req.getSession().setAttribute("film", f);
                rd.include(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        out.close();
    }
}
