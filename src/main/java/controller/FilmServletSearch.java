package controller;

import model.Film;
import sevices.FilmService;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Kevin Veld on 12-6-2016.
 */
public class FilmServletSearch extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        String zoekterm = req.getParameter("searchbar");
        String zoeken_op = req.getParameter("zoeken_op");
        FilmService filmService = new FilmService();
        PrintWriter out = resp.getWriter();
        resp.setContentType("text/html");
        if(zoekterm.equals("") || zoekterm.isEmpty()){
            RequestDispatcher rd = req.getRequestDispatcher("/gebruiker/FilmDB.jsp");
            out.println("<p class=\"message\"> U moet een zoekterm invoeren om te kunnen zoeken</p>");
            rd.include(req, resp);
        } else {
            try {
                RequestDispatcher rd = req.getRequestDispatcher("/gebruiker/FilmDB.jsp");
                List<Film> films = filmService.SearchFilm(zoekterm, zoeken_op);
                if(!films.isEmpty()) {
                    out.println("<p class=\"message\"> <b>zoekterm: '"+ zoekterm+ "' heeft de volgende resultaten opgeleverd</b></p>");
                    out.println("<form action=\"/gebruiker/FilmServletDetail.do\" method=\"POST\">");
                    for (Film f : films) {
                        out.println("<p class=\"message\"> De film '" + f.getNaam() + "' uit het jaar " + f.getJaartal() + " <button id=\"detail_button\" name=\"titel\" type=\"submit\" value=\""+f.getNaam()+"\">details</button></p>");
                    }
                    out.println("</form>");
                }
                else{
                    out.println("<p class=\"message\">Er zijn geen resultaten gevonden </p>");
                }
                rd.include(req, resp);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        out.close();
    }
}
