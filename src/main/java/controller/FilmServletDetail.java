package controller;

import model.Film;
import sevices.FilmService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Created by Kevin Veld on 13-6-2016.
 */
public class FilmServletDetail extends HttpServlet{
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        FilmService filmService = new FilmService();
        String filmtitel = req.getParameter("titel");
        Film film = filmService.SearchFilm(filmtitel,"naam").get(0);

        PrintWriter out = resp.getWriter();
        resp.setContentType("text/html");

        try {
            RequestDispatcher rd = req.getRequestDispatcher("/gebruiker/filmdetail.jsp");
            req.setAttribute("filmdetail", film);
            rd.forward(req, resp);


        } catch(Exception e){
            e.printStackTrace();
        }
        out.close();
    }
}
