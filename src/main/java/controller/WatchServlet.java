package controller;

import model.Gebruiker;
import model.Watch;
import sevices.FilmService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin Veld on 14-6-2016.
 */
public class WatchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        String watchstatus = req.getParameter("watchstatus");
        int film_id = Integer.parseInt(req.getParameter("film_id"));
        int gebruiker_id = Integer.parseInt(req.getParameter("gebruiker_id"));;
        FilmService filmService = new FilmService();
        PrintWriter out = resp.getWriter();
        resp.setContentType("text/html");

        try{
            Gebruiker u = (Gebruiker) req.getSession().getAttribute("gebruiker");
            RequestDispatcher rd = req.getRequestDispatcher("/gebruiker/watchlist.jsp");
            out.println("<p class=\"message\">De film is aan de lijst '"+watchstatus+"' toegevoegd </p>");

            boolean exist = false;
            for(Watch w : filmService.getWatchlists()){
                if(w.getGebruiker_id() == gebruiker_id){
                    if(w.getFilm_id() == film_id){
                        exist = true;
                    }
                }
            }
            if(exist){
                filmService.updateWatch(watchstatus, gebruiker_id, film_id);
            } else{
                filmService.addWatch(watchstatus, gebruiker_id, film_id);
            }

            List<Watch> userWatchlist = new ArrayList<Watch>();
            for(Watch w2 : filmService.getWatchlists()){
                if(w2.getGebruiker_id() == u.getGebruiker_id()){
                    userWatchlist.add(w2);
                }
            }
            req.getSession().setAttribute("watchlist", userWatchlist);
            rd.include(req, resp);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
