package utils;

import model.Gebruiker;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by Kevin Veld on 9-6-2016.
 */
public class FilmFilter implements Filter {
    public void init(FilterConfig arg0) throws ServletException {

    }
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException{
        HttpServletRequest r2 = (HttpServletRequest)req;
        Gebruiker gebruiker = (Gebruiker) r2.getSession().getAttribute("gebruiker");
        if(gebruiker == null){
            r2.getRequestDispatcher("/index.jsp").forward(req, resp);
        } else {
            chain.doFilter(req, resp);
        }
    }

    public void destroy(){

    }
}