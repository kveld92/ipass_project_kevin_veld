package utils;
/**
 * Created by Kevin Veld on 8-6-2016.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {

    private static Connection connection = null;

    public static Connection getConnection() {
        if (connection != null)
            return connection;
        else {
            try {
//                String driver = "com.mysql.jdbc.Driver";
//                String url = "jdbc:mysql://localhost:3306/filmdb";
//                String user = "root";
//                String password = "root";

                //openshift
                String driver = "com.mysql.jdbc.Driver";
                String url = "jdbc:mysql://127.10.134.2:3306/filmdb";
                String user = "adminrE9qlRX";
                String password = "tHA7J57sXV63";

                Class.forName(driver);
                connection = DriverManager.getConnection(url, user, password);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return connection;
        }

    }
}