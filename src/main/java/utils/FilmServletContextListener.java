package utils;

import sevices.FilmService;
import sevices.SeviceProvider;
import javax.servlet.*;

/**
 * Created by Kevin Veld on 8-6-2016.
 */
public class FilmServletContextListener implements ServletContextListener {
    public void contextInitialized(ServletContextEvent sce){
        ServletContext sc = sce.getServletContext();
        FilmService service = SeviceProvider.getFilmService();
        service.getAllGebruikers();
        sc.setAttribute("userlist", service);
//        System.out.println(service.getAllFilms());
//        System.out.println(service.getAllGebruikers());
//        System.out.println(service.getWatchlists());
    }

    public void contextDestroyed(ServletContextEvent sce){

    }

}
