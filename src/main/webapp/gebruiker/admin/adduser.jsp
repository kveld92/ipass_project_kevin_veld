<%--
  Created by IntelliJ IDEA.
  User: Kevin Veld
  Date: 13-6-2016
  Time: 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Gebruiker toevoegen</title>
    <link rel="stylesheet" type="text/css" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
<%@ include file="/includes/banner.jsp" %>

    <div class="container1">
        <h2>Gebruiker toevoegen</h2>
        <div class="film_form">
            <form action="/gebruiker/UserServletAdd.do" method="POST">
                Gebruikersnaam: <input type="text" name="uname" pattern="^[a-zA-Z0-9]*" maxlength="15" required="required"/></br>
                Wachtwoord: <input type="password" name="pass" pattern="^[a-zA-Z0-9]*" maxlength="15" required="required"/></br>
                Herhaal Wachtwoord: <input type="password" name="pass2" pattern="^[a-zA-Z0-9]*" maxlength="15" required="required"/></br>
                Type-account:

                <select name="user_type">
                    <option value="0">Kijker</option>
                    <option value="1">Admin</option>
                </select>
                </br>

                <input type="submit" value="Toevoegen"/>
            </form>
        </div>
    </div>

</body>
</html>
