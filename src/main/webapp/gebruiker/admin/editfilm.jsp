<%@ page import="sevices.FilmService" %>
<%@ page import="model.Film" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kevin Veld
  Date: 11-6-2016
  Time: 20:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Film wijzigen</title>
    <link rel="stylesheet" type="text/css" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <script src="https://code.jquery.com/jquery-2.2.3.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#filmedit_form').hide();
            $('select').on('change', function(){
                if($('select').val() === ""){
                    $('#filmedit_form').hide();
                } else{
                    $('select').hide();
                    $('#pmessage').text("Vul de gegevens naar wens aan en druk vervolgens op 'Film wijzigen' om de film aan te passen.");
                    $('#filmedit_form').show();
                    $("#naam").attr({
                        "value" : $('select').val()
                    })
                }
            });
            $('#filmedit_form').submit(function() {
                return confirm("U staat op het punt een film te wijzigen. Druk op OK om door te gaan");
            });
        });
    </script>
</head>
<body>
<%@ include file="/includes/banner.jsp" %>

    <div class="container1">
        <h2>Film wijzigen</h2>
        <p id="pmessage"> Kies een film om te wijzigen: </p >
        </br>
        <select>
                <option value=""></option>
            <c:forEach var="i" items="${film}">
                <option value="${i.naam}">${i.naam}</option>
            </c:forEach>
        </select>
        </br></br>
        <div class="film_form">
            <form id= "filmedit_form" action="/gebruiker/FilmServletEdit.do" method="POST">
                Naam: </br> <input id="naam" type="text" name="naam" maxlength="45" pattern="^[a-zA-Z0-9 :!-]*" required="required"/> </br>
                Jaartal:  </br> <input  type="number" min = 0 name="jaartal" maxlength="4"required="required"/> </br>
                Genre:  </br> <input type="text" name="genre"  maxlength="45" pattern="^[a-zA-Z0-9 -]*" required="required"/> </br>
                BoxNr:  </br> <input  type="number" min = 0 name="box_nr" required="required"/> </br>
                PlaatsNr:  </br> <input  type="number" min = 0 name="plaats_nr" required="required"/> </br>
                Samenvatting:  </br>  <textarea rows ="5" cols="25" name="samenvatting" maxlength="254" pattern="^[a-zA-Z0-9 ]*" required="required"></textarea></br>
                </br><input type="submit" value="Film wijzigen"/>
            </form>
        </div>
    </div>
</body>
</html>
