<%--
  Created by IntelliJ IDEA.
  User: Kevin Veld
  Date: 11-6-2016
  Time: 23:59
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Film wijzigen</title>
    <link rel="stylesheet" type="text/css" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <script src="https://code.jquery.com/jquery-2.2.3.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#filmdel_form').submit(function() {
                return confirm("U staat op het punt een film te verwijderen. Druk op OK om door te gaan");
            });
        });
    </script>
</head>
<body>
<%@ include file="/includes/banner.jsp" %>



<div class="container1">
    <h2>Film wijzigen</h2>
    <p>Kies een film om te verwijderen en druk vervolgens op 'Film verwijderen' om hem te verwijderen: </p >
    </br>
    <div class="film_form">
        <form id= "filmdel_form" action="/gebruiker/FilmServletDelete.do" method="POST">
            <select name="cb_del">
                <option value=""></option>
                <c:forEach var="i" items="${film}">
                    <option value="${i.naam}">${i.naam}</option>
                </c:forEach>
            </select>
            </br>
            </br><input type="submit" value="Film verwijderen"/>
        </form>
    </div>
</div>
</body>
</html>
