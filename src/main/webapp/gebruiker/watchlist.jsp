<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kevin Veld
  Date: 9-6-2016
  Time: 18:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Watchlist</title>
    <link rel="stylesheet" type="text/css" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
<%@ include file="/includes/banner.jsp" %>
    <div class="post">
        <h2>Want-to-watch</h2>
        <table>
            <tr><th>Titel</th><th>Jaartal</th><th>Genre</th><th>BoxNr</th><th>PlaatsNr</th><th>Overzicht</th></tr>
            <c:forEach var="i" items="${watchlist}">
                    <c:forEach var="y" items="${film}">
                        <c:if test="${i.film_id==y.film_id}">
                            <c:if test="${i.watch_status eq 'want-to-watch'}">
                                <form action="/gebruiker/FilmServletDetail.do" method="POST">
                                    <tr><td><c:out value="${y.naam}"/></td><td><c:out value="${y.jaartal}"/></td><td><c:out value="${y.genre}"/></td><td><c:out value="${y.box_nr}"/></td><td><c:out value="${y.plaats_nr}"/></td><td><button id="detail_button" name="titel" type="submit" value="<c:out value="${y.naam}"/>">details</button></td></tr>
                                </form>
                            </c:if>
                        </c:if>
                    </c:forEach>
            </c:forEach>
        </table>

        <h2>Bekeken</h2>
        <table>
            <tr><th>Titel</th><th>Jaartal</th><th>Genre</th><th>BoxNr</th><th>PlaatsNr</th><th>Overzicht</th></tr>
            <c:forEach var="i" items="${watchlist}">
                <c:forEach var="y" items="${film}">
                    <c:if test="${i.film_id==y.film_id}">
                        <c:if test="${i.watch_status eq 'bekeken'}">
                            <form action="/gebruiker/FilmServletDetail.do" method="POST">
                                <tr><td><c:out value="${y.naam}"/></td><td><c:out value="${y.jaartal}"/></td><td><c:out value="${y.genre}"/></td><td><c:out value="${y.box_nr}"/></td><td><c:out value="${y.plaats_nr}"/></td><td><button id="detail_button" name="titel" type="submit" value="<c:out value="${y.naam}"/>">details</button></td></tr>
                            </form>
                        </c:if>
                    </c:if>
                </c:forEach>
            </c:forEach>
        </table>
    </div>
</body>
</html>
