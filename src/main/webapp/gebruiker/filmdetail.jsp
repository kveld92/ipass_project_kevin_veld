<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kevin Veld
  Date: 13-6-2016
  Time: 21:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Filmdetail</title>
    <link rel="stylesheet" type="text/css" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
    <%@ include file="/includes/banner.jsp" %>
    <div class="post">
        <h2><c:out value="${filmdetail.naam}"/> (<c:out value="${filmdetail.jaartal}"/>)</h2></br>
        <div class ="container1">
            <p id="genre"><c:out value="${filmdetail.genre}"/> </p></br>
            <p id="samenvatting">Samenvatting: </br><c:out value="${filmdetail.samenvatting}"/> </p> </br>
            <p id="boxnr">BoxNr: </br><c:out value="${filmdetail.box_nr}"/> </p></br>
            <p id="plaatsnr">PlaatsNr: </br><c:out value="${filmdetail.plaats_nr}"/></p>
        </div></br>
        <h2>Aan lijst toevoegen</h2>
        <div class="container1">
            <p> Hier kunt u de film aan de lijst want-to-watch of bekeken toevoegen.</p>
            <form action="/gebruiker/WatchServlet.do" method="POST">
                <input type="hidden" value="<c:out value="${filmdetail.film_id}"/>" name="film_id"/>
                <input type="hidden" value="<c:out value="${gebruiker.gebruiker_id}"/>" name="gebruiker_id"/>
                <select name="watchstatus">
                    <option value=""></option>
                    <option value="want-to-watch">Want-to-watch</option>
                    <option value="bekeken">Bekeken</option>
                </select>
                <input type="submit" value="Opslaan"/>
            </form>
        </div>
    </div>
</body>
</html>
